import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private static final double CONFIDENCE_95 = 1.96;
    private int trials;
    private double mean;
    private double stddev;

    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }

        this.trials = trials;
        double[] percolationThreshold = new double[trials];

        for (int k = 0; k < trials; k++) {
            Percolation percolation = new Percolation(n);

            while (!percolation.percolates()) {

                while (true) {
                    int rand = StdRandom.uniformInt(n * n);
                    int row = rand / n + 1;
                    int col = rand % n + 1;

                    if (!percolation.isOpen(row, col)) {
                        percolation.open(row, col);
                        break;
                    }
                }
            }

            percolationThreshold[k] = (double) percolation.numberOfOpenSites() / (n * n);
        }

        mean = StdStats.mean(percolationThreshold);
        stddev = StdStats.stddev(percolationThreshold);
    }

    public double mean() {
        return mean;
    }

    public double stddev() {
        return stddev;
    }

    public double confidenceLo() {
        return mean - CONFIDENCE_95 * stddev / Math.sqrt(trials);
    }

    public double confidenceHi() {
        return mean + CONFIDENCE_95 * stddev / Math.sqrt(trials);
    }

    public static void main(String[] args) {
        PercolationStats percolationStats = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));

        System.out.printf("%-23s = %f%n", "mean", percolationStats.mean());
        System.out.printf("%-23s = %f%n", "stddev", percolationStats.stddev());
        System.out.printf("%-23s = [%f, %f]%n", "95% confidence interval", percolationStats.confidenceLo(), percolationStats.confidenceHi());

    }
}
