import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private boolean[][] sites;
    private int n;
    private int openSites;
    private WeightedQuickUnionUF nodes;

    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        }

        sites = new boolean[n][n];
        this.n = n;
        openSites = 0;
        nodes = new WeightedQuickUnionUF(n * n + 2);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                sites[i][j] = false;
            }
        }
    }

    private int twoDimToOneDim(int row, int col) {
        return (row - 1) * n + col - 1;
    }

    public void open(int row, int col) {
        if (row < 1 || row > n || col < 1 || col > n) {
            throw new IllegalArgumentException();
        }

        if (!isOpen(row, col)) {
            sites[row - 1][col - 1] = true;
            openSites++;

            if (row == 1) {
                nodes.union(twoDimToOneDim(row, col), n * n);
            }

            if (row == n) {
                nodes.union(twoDimToOneDim(row, col), n * n + 1);
            }

            for (int dx = (row > 1 ? -1 : 0); dx <= (row < n ? 1 : 0); ++dx) {
                for (int dy = (col > 1 ? -1 : 0); dy <= (col < n ? 1 : 0); ++dy) {
                    if (!((dx == -1 && dy == -1) || (dx == 1 && dy == -1) || (dx == -1 && dy == 1) || (dx == 1 && dy == 1) || (dx == 0 && dy == 0))) {
                        if (isOpen(row + dx, col + dy)) {
                            nodes.union(twoDimToOneDim(row, col), twoDimToOneDim(row + dx, col + dy));
                        }
                    }
                }
            }
        }
    }

    public boolean isOpen(int row, int col) {
        if (row < 1 || row > n || col < 1 || col > n) {
            throw new IllegalArgumentException();
        }

        return sites[row - 1][col - 1];
    }

    public boolean isFull(int row, int col) {
        if (row < 1 || row > n || col < 1 || col > n) {
            throw new IllegalArgumentException();
        }

        return isOpen(row, col) && nodes.find(twoDimToOneDim(row, col)) == nodes.find(n * n);
    }

    public int numberOfOpenSites() {
        return openSites;
    }

    public boolean percolates() {
        return nodes.find(n * n) == nodes.find(n * n + 1);
    }
}
