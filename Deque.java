import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

  private LinkedList<Item> dequeList;

  public Deque() {
    dequeList = new LinkedList<Item>();
  }

  public boolean isEmpty() {
    return dequeList.isEmpty();
  }

  public int size() {
    return dequeList.size();
  }

  public void addFirst(Item item) {
    if (item == null) {
      throw new IllegalArgumentException();
    }

    dequeList.addFirst(item);
  }

  public void addLast(Item item) {
    if (item == null) {
      throw new IllegalArgumentException();
    }

    dequeList.addLast(item);
  }

  public Item removeFirst() {
    if (dequeList.isEmpty()) {
      throw new NoSuchElementException();
    }

    return dequeList.removeFirst();
  }

  public Item removeLast() {
    if (dequeList.isEmpty()) {
      throw new NoSuchElementException();
    }

    return dequeList.removeLast();
  }

  public Iterator<Item> iterator() {
    Iterator<Item> iter = dequeList.listIterator();

    return iter;
  }

  public static void main(String[] args) {
    Deque<Integer> deque = new Deque<Integer>();

    deque.addFirst(1);
    deque.addFirst(2);

    System.out.println("deque is empty? " + deque.isEmpty());
    System.out.println("The size of deque: " + deque.size());
    System.out.println("The first element (being removed): " + deque.removeFirst());
    System.out.println("The last element (being removed): " + deque.removeLast());

    deque.addFirst(3);
    deque.addLast(4);
    Iterator<Integer> iter = deque.iterator();

    System.out.println("deque elements after second insertion:");
    while (iter.hasNext()) {
      System.out.println(iter.next());
    }
  }
}
